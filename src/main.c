/**
 ******************************************************************************
 * @file    main.c
 * @author  Vojtech Mrazek
 * @version V1.0.1
 * @date    15-May-2012
 * @brief   Main program body
 ******************************************************************************
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "config.h"
#include "assert.h"

/* USB includes */
#include "usbd_cdc_core.h"
#include "usbd_cdc.h"
#include "usbd_usr.h"
#include "usbd_desc.h"

#include "stm32f4xx_gpio.h"
#include "stm32f4xx_can.h"
//#define NO_USB
uint32_t TimingDelay = 0;

__ALIGN_BEGIN USB_OTG_CORE_HANDLE USB_OTG_dev __ALIGN_END;

#define BYTE2UINT16(arr, offset) ((*(arr + offset) << 24) | (*(arr + offset + 1) << 16) \
								 | (*(arr + offset + 2) << 8) | (*(arr + offset + 3)))

/* Private functions ---------------------------------------------------------*/
void CAN_InitMsg(CanRxMsg *Message);
void CAN_DropFIFO(void);
void NVIC_Config(void);
void CAN_SetFilter();
void CAN_SetParameters(uint16_t prescaler, uint8_t SJW, uint8_t BS1,
		uint8_t BS2);
void CAN_Config(void);
void Clk_Init(void);

#ifdef NO_USB
uint16_t pres;
uint8_t sjw, bs1, bs2;
#endif

/**
 * @brief  Initializes the CAN Message.
 * @param  Message: pointer to the message to initialize
 * @retval None
 */
void CAN_InitMsg(CanRxMsg *Message)
{
	uint8_t i = 0;

	Message->StdId = 0x00;
	Message->ExtId = 0x00;
	Message->IDE = CAN_ID_STD;
	Message->DLC = 0;
	Message->FMI = 0;
	for (i = 0; i < 8; i++)
	{
		Message->Data[i] = 0x00;
	}
}

/**
 * @brief Handling data reception from USB
 */
void USB_DataRx(uint8_t* Buf, uint32_t Len)
{
	uint8_t i;
	uint32_t pos = 0;

	while (pos < Len) /* There can be more packets in the buffer */
	{
		/* Send data CMD */
		if (pos + Len >= PROT_LEN_SEND && *Buf == PROT_CMD_SEND)
		{
			CanTxMsg msg;
			msg.StdId = (*(Buf + 1) << 24) | (*(Buf + 2) << 16)
					| (*(Buf + 3) << 8) | (*(Buf + 4));
			msg.ExtId = 0;
			msg.IDE = CAN_ID_STD;
			msg.RTR = CAN_RTR_DATA;
			msg.DLC = *(Buf + 5);
			for (i = 0; i < 8; i++)
				msg.Data[i] = *(Buf + 67 + i);
			if (CAN_Transmit(CANx, &msg) == CAN_TxStatus_NoMailBox)
			{
				CAN_CancelTransmit(CANx, 0);
				CAN_CancelTransmit(CANx, 1);
				CAN_CancelTransmit(CANx, 2);
			}

			/* Move to next packet */
			pos += PROT_LEN_SEND;
			Buf += PROT_LEN_SEND;
		}
		/* Stop capturing */
		else if (pos + Len >= PROT_LEN_STOP && *Buf == PROT_CMD_STOP)
		{
			/* Disable FIFO message pending Interrupt */
			CAN_ITConfig(CANx, CAN_FIFO_IN, DISABLE);

			/* Move to next packet */
			pos += PROT_LEN_STOP;
			Buf += PROT_LEN_STOP;
		}

		/* Start capturing */
		else if (pos + Len >= PROT_LEN_START && *Buf == PROT_CMD_START)
		{
			/* Clear FIFO */
			CAN_DropFIFO();

			/* Enable FIFO message pending Interrupt */
			CAN_ITConfig(CANx, CAN_FIFO_IN, ENABLE);

			/* Move to next packet */
			pos += PROT_LEN_START;
			Buf += PROT_LEN_START;
		}

		/* Set CAN parameters */
		else if (pos + Len >= PROT_LEN_SET && *Buf == PROT_CMD_SET)
		{
			/* -1 is included because of XXX_1tq equals 0 */
			CAN_SetParameters(Buf[1], Buf[2] - 1, Buf[3] - 1, Buf[4] - 1);
			CAN_SetFilter();

			/* Move to next packet */
			pos += PROT_LEN_SET;
			Buf += PROT_LEN_SET;
		}

		/* Add filter */
		else if (pos + Len >= PROT_LEN_FILT_ADD && *Buf == PROT_CMD_FILT_ADD)
		{
			CAN_FilterInitTypeDef CAN_FilterInitStructure;
			CAN_FilterInitStructure.CAN_FilterNumber = CAN_FILTER_NMR + Buf[1];
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO;
			CAN_FilterInitStructure.CAN_FilterMode = Buf[18];
			CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
			CAN_FilterInitStructure.CAN_FilterIdHigh = BYTE2UINT16(Buf, 2);
			CAN_FilterInitStructure.CAN_FilterIdLow = BYTE2UINT16(Buf, 6);
			CAN_FilterInitStructure.CAN_FilterMaskIdHigh = BYTE2UINT16(Buf, 10);
			CAN_FilterInitStructure.CAN_FilterMaskIdLow = BYTE2UINT16(Buf, 14);
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO_ID;
			CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
			CAN_FilterInit(&CAN_FilterInitStructure);


			/* Move to next packet */
			pos += PROT_LEN_FILT_ADD;
			Buf += PROT_LEN_FILT_ADD;
		}

		/* Remove filter */
		else if (pos + Len >= PROT_LEN_FILT_OFF && *Buf == PROT_CMD_FILT_OFF)
		{
			CAN_FilterInitTypeDef CAN_FilterInitStructure;
			CAN_FilterInitStructure.CAN_FilterNumber = CAN_FILTER_NMR + Buf[1];
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO;
			CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
			CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
			CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
			CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
			CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
			CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
			CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO_ID;
			CAN_FilterInitStructure.CAN_FilterActivation = DISABLE;
			CAN_FilterInit(&CAN_FilterInitStructure);

			/* Move to next packet */
			pos += PROT_LEN_FILT_OFF;
			Buf += PROT_LEN_FILT_OFF;
		}

		/* Reset Filter */
		else if (pos + Len >= PROT_LEN_FILT_RESET && *Buf == PROT_CMD_FILT_RESET)
		{
			CAN_SetFilter();

			/* Move to next packet */
			pos += PROT_LEN_FILT_RESET;
			Buf += PROT_LEN_FILT_RESET;
		}
		else
			/* Unknown command */
			return;
	} /* while(pos < len) */
}

/***
 * @brief Clear all messages pending in FIFO
 */
void CAN_DropFIFO(void)
{
	CanRxMsg msgrx;
	while (CAN_MessagePending(CANx, CAN_FIFO) != 0)
	{
		CAN_Receive(CANx, CAN_FIFO, &msgrx);
	}
}

/**
 * @brief IRQ from CAN_FIFO, received data are sent to USB device
 */
void CAN_IRQ_HANDLER(void)
{
	uint8_t Buf[PROT_LEN_SEND];
	CanRxMsg msgrx;
	CAN_InitMsg(&msgrx);
	while (CAN_MessagePending(CANx, CAN_FIFO) != 0)
	{
		CAN_Receive(CANx, CAN_FIFO, &msgrx);

		Buf[0] = PROT_CMD_SEND; // Code
		Buf[1] = (uint8_t) ((msgrx.StdId & 0xFF000000) >> 24);
		Buf[2] = (uint8_t) ((msgrx.StdId & 0x00FF0000) >> 16);
		Buf[3] = (uint8_t) ((msgrx.StdId & 0x0000FF00) >> 8);
		Buf[4] = (uint8_t) ((msgrx.StdId & 0x000000FF));
		Buf[5] = msgrx.DLC;

		uint8_t i;
		for (i = 0; i < 8; i++)
		{
			Buf[6 + i] = msgrx.Data[i];
		}
#ifndef NO_USB
		cdc_DataTx(Buf, PROT_LEN_SEND);
#endif
	}
}

/**
 * @brief Setup NVIC priority
 */
void NVIC_Config(void)
{
	/* Setup CAN FIFO RX priority */
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = CAN_IRQ;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/**
 * @brief Set up CANx device, in case of too big value nothing is initialised
 * @param SJW  		!< Specifies the maximum number of time quanta
 *                     the CAN hardware is allowed to lengthen or
 *                     shorten a bit to perform resynchronization.
 *                     This parameter can be a value of @ref CAN_synchronisation_jump_width
 *                     Maximum value is 0x03
 *
 * @param BS1       !< Specifies the number of time quanta in Bit
 *                     Segment 1. This parameter can be a value of
 *                     @ref CAN_time_quantum_in_bit_segment_1
 *                     Maximum value is 0x0F
 *
 * @param BS2       !< Specifies the number of time quanta in Bit Segment 2.
 *                     This parameter can be a value of @ref CAN_time_quantum_in_bit_segment_2
 *                     Maximum value is 0x07
 */
void CAN_SetParameters(uint16_t prescaler, uint8_t SJW, uint8_t BS1,
		uint8_t BS2)
{
	/* Test parameter values */
	if (SJW > 0x03 || BS1 > 0x0F || BS2 > 0x07)
		return;
	/* Initialisation of CAN */
	CAN_DeInit(CANx);
	CAN_InitTypeDef CAN_InitStructure;
	CAN_StructInit(&CAN_InitStructure);
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = ENABLE; // Automatic bus off managment
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = ENABLE;
	CAN_InitStructure.CAN_Prescaler = prescaler; // 9 for 30 MHz,
	CAN_InitStructure.CAN_SJW = SJW;
	CAN_InitStructure.CAN_BS1 = BS1;
	CAN_InitStructure.CAN_BS2 = BS2;
#ifdef CAN_LOOPBACK
	CAN_InitStructure.CAN_Mode = CAN_Mode_LoopBack;
#else
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
#endif
	uint8_t status = CAN_Init(CANx, &CAN_InitStructure);
	while (status != CAN_InitStatus_Success)
		;
}

/**
 * @brief Default filter - accept all to CAN_FIFO
 */
void CAN_SetFilter()
{
	/* Default filter - accept all to CAN_FIFO*/
	CAN_FilterInitTypeDef CAN_FilterInitStructure;
	CAN_FilterInitStructure.CAN_FilterNumber = CAN_FILTER_NMR;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO_ID;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO message pending Interrupt */
	CAN_ITConfig(CANx, CAN_FIFO_IN, ENABLE);
}

/***
 * @brief Setup CAN port
 */
void CAN_Config(void)
{
	/* Enable the CAN controller interface clock  */
	RCC_AHB1PeriphClockCmd(CAN_GPIO_CLK, ENABLE);
	RCC_APB1PeriphClockCmd(CAN_CLK, ENABLE);

	/* CAN pins configuration */
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN | CAN_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; // Push - pull
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(CAN_GPIO_PORT, &GPIO_InitStructure);

	GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_TX_SOURCE, CAN_AF_PORT);
	GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_RX_SOURCE, CAN_AF_PORT);

	/* Initialisation of CAN */
	CAN_SetParameters(6, CAN_SJW_2tq, CAN_BS1_5tq, CAN_BS2_8tq);

	/* Default filter - accept all to CAN_FIFO*/
	CAN_SetFilter();
}

void Clk_Init(void)
{
	// 1. Clocking the controller from internal HSI RC (8 MHz)
	RCC_HSICmd(ENABLE);
	// wait until the HSI is ready
	while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
		;
	RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);
	// 2. Enable ext. high frequency OSC
	RCC_HSEConfig(RCC_HSE_ON);
	// wait until the HSE is ready
	while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET)
		;
	// 3. Init PLL
	//RCC_PLLConfig(RCC_PLLSource_HSE_Div1,RCC_PLLMul_9); // 72MHz
	RCC_PLLCmd(ENABLE);
	// wait until the PLL is ready
	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
		;
	// 4. Set system clock dividers
	//RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_1Div5);
	//RCC_ADCCLKConfig(RCC_PCLK2_Div8);
	RCC_PCLK2Config(RCC_HCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div2);
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
#ifdef EMB_FLASH
	// 5. Init Embedded Flash
	// Zero wait state, if 0 < HCLK 24 MHz
	// One wait state, if 24 MHz < HCLK 56 MHz
	// Two wait states, if 56 MHz < HCLK 72 MHz
	// Flash wait state
	FLASH_SetLatency(FLASH_Latency_2);
	// Half cycle access
	FLASH_HalfCycleAccessCmd(FLASH_HalfCycleAccess_Disable);
	// Prefetch buffer
	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
#endif // EMB_FLASH
	// 5. Clock system from PLL
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
}

/**
 * @brief   Main program
 * @param  None
 * @retval None
 */
int main(void)
{
#if false
	TIM_ICInitTypeDef timInit;
	timInit.TIM_Channel=1;
	timInit.TIM_ICPrescaler=TIM_ICPSC_DIV1;
	timInit.TIM_ICFilter=0;
	timInit.TIM_ICPrescaler=TIM_ICSelection_TRC;

	TIM_ICInit(TIM1, &timInit);
#endif

	USBD_Init(&USB_OTG_dev,
#ifdef USE_USB_OTG_HS
			USB_OTG_HS_CORE_ID,
#else
			USB_OTG_FS_CORE_ID,
#endif
			&USR_desc,
			&USBD_CDC_cb,
			&USR_cb);
	Clk_Init();

	/* SysTick end of count event each 10ms */
	SysTick_Config(SystemCoreClock / 100);
	NVIC_Config();

	CAN_Config();

#ifdef NO_USB
	pres=12;
	sjw=0;
	bs1=7;
	bs2=4;
	STM_EVAL_LEDInit(LED6);
	CAN_SetParameters(pres, sjw, bs1, bs2);
	CAN_SetFilter();
#endif

	while (1)
	{
#ifdef NO_USB
		Delay(100);
		CanTxMsg msg;
		msg.StdId = 0x080;
		msg.ExtId = 0;
		msg.IDE = CAN_ID_STD;
		msg.RTR = CAN_RTR_DATA;
		msg.DLC = 0x04;
		msg.Data[0] = 0x00;
		msg.Data[1] = 0x00;
		msg.Data[2] = 0x07;
		msg.Data[3] = 0x00;

		if (CAN_Transmit(CANx, &msg) == CAN_TxStatus_NoMailBox)
		{
			if(++bs2>0x07)
			{
				bs2=1;
				if(++bs1>0x0F)
				{
					bs1=1;
					if(++sjw>0x03)
					{
						sjw=1;
						if(++pres>60)
						{
							pres++;
						}
					}
				}
			}
			STM_EVAL_LEDToggle(LED6);
			CAN_CancelTransmit(CANx, 0);
			CAN_CancelTransmit(CANx, 1);
			CAN_CancelTransmit(CANx, 2);
			CAN_SetParameters(pres, sjw, bs1, bs2);
			CAN_SetFilter();
		}

#else
		/* Every KEEP_ALIVE_TIMEOUT send KEEP-ALIVE packet to USB*/
		Delay(KEEP_ALIVE_TIMEOUT);

		uint8_t packet = PROT_CMD_KA;
		assert(PROT_LEN_KA == 1);
		cdc_DataTx(&packet, PROT_LEN_KA);
#endif

	}
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in milliseconds.
 * @retval None
 */
void Delay(__IO uint32_t nTime)
{
	TimingDelay = nTime;

	while (TimingDelay != 0)
		;
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
	if (TimingDelay != 0x00)
	{
		TimingDelay--;
	}
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
	{
	}
}
#endif

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
