/*
 * config.h
 *
 *  Created on: Nov 23, 2012
 *      Author: Vojta
 *  Configuration of CAN device
 */

#ifndef CONFIG_H_
#define CONFIG_H_

/* CAN Settings */
#define USE_CAN1
//#define CAN_LOOPBACK
#define USE_FIFO0

/* Keep-alive packet timeout in ms */
#define KEEP_ALIVE_TIMEOUT 100

#ifdef  USE_CAN1
#define CANx                       CAN1
#define CAN_FILTER_NMR	           0
#define CAN_CLK                    RCC_APB1Periph_CAN1
#define CAN_RX_PIN                 GPIO_Pin_0
#define CAN_TX_PIN                 GPIO_Pin_1
#define CAN_GPIO_PORT              GPIOD
#define CAN_GPIO_CLK               RCC_AHB1Periph_GPIOD
#define CAN_AF_PORT                GPIO_AF_CAN1
#define CAN_RX_SOURCE              GPIO_PinSource0
#define CAN_TX_SOURCE              GPIO_PinSource1
#else /*USE_CAN2*/
#define CANx                       CAN2
#define USE_CAN2
#define CAN_FILTER_NMR	           14
#define CAN_CLK                    (RCC_APB1Periph_CAN1 | RCC_APB1Periph_CAN2)
#define CAN_RX_PIN                 GPIO_Pin_5
#define CAN_TX_PIN                 GPIO_Pin_13
#define CAN_GPIO_PORT              GPIOB
#define CAN_GPIO_CLK               RCC_AHB1Periph_GPIOB
#define CAN_AF_PORT                GPIO_AF_CAN2
#define CAN_RX_SOURCE              GPIO_PinSource5
#define CAN_TX_SOURCE              GPIO_PinSource13
#endif  /* USE_CAN1 */

#ifdef USE_FIFO0
#define CAN_FIFO_ID				   0
#define CAN_FIFO                   CAN_FIFO0
#define CAN_FIFO_IN                CAN_IT_FMP0
#ifdef USE_CAN1
#define CAN_IRQ                    CAN1_RX0_IRQn
#define CAN_IRQ_HANDLER            CAN1_RX0_IRQHandler
#else /* USE_CAN2 */
#define CAN_IRQ                    CAN2_RX0_IRQn
#define CAN_IRQ_HANDLER            CAN2_RX0_IRQHandler
#endif /* USE_CAN1 */
#else /* USE_FIFO1 */
#error "Not implemented yet"
#endif

/* Protocol definition */
#define PROT_CMD_SEND 0x01
#define PROT_LEN_SEND 14
#define PROT_CMD_START 0x03
#define PROT_LEN_START 1
#define PROT_CMD_STOP 0x02
#define PROT_LEN_STOP 1
#define PROT_CMD_SET 0x04
#define PROT_LEN_SET 5
#define PROT_CMD_KA 0x05
#define PROT_LEN_KA 1

#define PROT_CMD_FILT_ADD  0x06
#define PROT_LEN_FILT_ADD  19
#define PROT_CMD_FILT_OFF  0x07
#define PROT_LEN_FILT_OFF  2
#define PROT_CMD_FILT_RESET  0x08
#define PROT_LEN_FILT_RESET  1

#endif /* CONFIG_H_ */
